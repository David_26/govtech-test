/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: [
      'images6.alphacoders.com',
      's4.anilist.co'
    ]
  }
}

module.exports = nextConfig
