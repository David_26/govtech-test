import MainHeader from '@components/headers/mainHeader';
import Card from '@elements/card';
import Skeleton from '@elements/skeleton';
import { getAnimeList, reqOptions, getGenres, baseUrl } from '@api/anime';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router'
import InfiniteScroll from 'react-infinite-scroll-component';

export default function Trending({list, pageInfo, genres}) {
  const router = useRouter();

  const [animeList, setAnimeList] = useState([]);
  const [pagination, setPagination] = useState({});
  const [onLoading, setOnLoading] = useState(true);
  const [genreList, setGenreList] = useState([]);

  const getList = async (genre) => {
    const newPage = pagination.hasNextPage ? pagination.currentPage + 1 : 1

    try {
      await fetch(baseUrl, reqOptions(getAnimeList(newPage, genre)))
        .then((result) => result.text())
        .then((response) => {
          const data = JSON.parse(response)
          setOnLoading(false)
          if (genre) {
            setAnimeList(data.data.Page.media)
          } else {
            setAnimeList([...animeList, ...data.data.Page.media])  
          }
          setPagination(data.data.Page.pageInfo)
        });
    } catch (err) {
      setOnLoading(false)
      console.log('err', err)
    }    
  }

  const handleChangeGenre = (val) => {
    setOnLoading(true);
    getList(val);
  }

  useEffect(() => {
    if (router.isReady) {
      setOnLoading(false)
      setAnimeList(list)
      setPagination(pageInfo)
      setGenreList(genres)
    }
  },[router.isReady, list, pageInfo])

  return (
    <>
      <MainHeader filterData={genreList} handleChangeGenre={handleChangeGenre}/>
      <main className="container mx-auto pt-28">
        <div className="mx-3 md:mx-72">
          <InfiniteScroll
            dataLength={animeList.length}
            next={getList}
            hasMore={pagination.hasNextPage}
            loader={<Skeleton/>}
            endMessage={
              <p>no more item</p>
            }
          >
            {onLoading &&
              [1,2,3,4].map(index => (
                <Skeleton key={index}/>
              ))
            }
            {!onLoading && animeList.length > 0 && (
            animeList.map((item) => (
              <div key={item.id} className="mb-5">
                <Card key={item.id} data={item}/>
              </div>
            ))
          )}
          </InfiniteScroll>
        </div>
      </main>
    </>
  )
}

export async function getServerSideProps() {
  const [ getList, getGenreList ] = await Promise.all([
    fetch(baseUrl, reqOptions(getAnimeList(1, "Action", "[POPULARITY_DESC, SCORE_DESC]"))),
    fetch(baseUrl, reqOptions(getGenres()))
  ]);

  const [ animeList, genreList ] = await Promise.all([
    getList.json(),
    getGenreList.json()
  ]);

  return {
    props: {
      list: animeList?.data?.Page?.media || null,
      pageInfo: animeList?.data?.Page?.pageInfo || 1,
      genres: genreList?.data?.GenreCollection || []
    }
  };
}