import MainHeader from '@components/headers/mainHeader';
import Card from '@elements/card';
import Skeleton from '@elements/skeleton';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router'

export default function Bookmark() {
  const router = useRouter();

  const [animeList, setAnimeList] = useState([]);
  const [onLoading, setOnloading] = useState(true);

  const getSavedAnime = () => {
    const savedList = localStorage.getItem("savedAnime");
    if (savedList) {
      const animeList = JSON.parse(savedList)
      setAnimeList(animeList)
    }
    setOnloading(false)
  }

  useEffect(() => {
    if (router.isReady) {
      getSavedAnime()
    }
  },[router.isReady])

  return (
    <>
      <MainHeader myBookmark/>
      <main className="container mx-auto pt-28">
        <div className="mx-3 md:mx-72">
          {onLoading &&
              [1,2,3,4].map(index => (
                <Skeleton key={index}/>
              ))
            }
            {!onLoading && animeList.length > 0 && (
            animeList.map((item) => (
              <div key={item.id} className="mb-5">
                <Card key={item.id} data={item}/>
              </div>
            ))
          )}
        </div>
      </main>
    </>
  )
}
