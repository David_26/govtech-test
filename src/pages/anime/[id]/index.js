// import MainHeader from '@components/headers/mainHeader';
import Cover from '@components/cover';
import { getAnimeDetail, reqOptions, baseUrl } from '@api/anime';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';

export default function Home({ detail }) {
  const router = useRouter();

  const [savedAnime, setSavedAnime] = useState([])
  const [isSaved, setIsSaved] = useState(false)

  const handleBookmark = (val, isSaved) => {
    let newArr = []

    if (isSaved) {
      handleCheckIsSaved(savedAnime, val)
    } else {
      if (savedAnime.length) {
        newArr = [...savedAnime, val]
      } else {
        newArr = [val]
      }

      setSavedAnime(newArr)
      localStorage.setItem("savedAnime", JSON.stringify(newArr));
      handleCheckIsSaved(newArr)
    }
  }

  const handleCheckSavedList = () => {
    const savedList = localStorage.getItem("savedAnime");
    if (savedList) {
      const animeList = JSON.parse(savedList)
      setSavedAnime(animeList)
      handleCheckIsSaved(animeList)
    }
  }

  const handleCheckIsSaved = (list, deleteVal) => {
    let updateStatus
    const checkAnime = list.find(item => item.id === detail.id)

    if (deleteVal) {
      const updateList = list.splice(checkAnime.id, 1)
      setSavedAnime(updateList)
      localStorage.setItem("savedAnime", JSON.stringify(updateList));
      updateStatus = false
    } else {
      updateStatus = checkAnime ? true : false
    }

    setIsSaved(updateStatus)
  }

  useEffect(() => {
    if (router.isReady && detail) {
      handleCheckSavedList() 
    }
  },[router.isReady, detail])

  return (
    <main className="h-screen h-screen justify-between flex flex-col">
      <div className="mb-auto md:hidden"></div>
      <Cover data={detail} handleBookmark={handleBookmark} isSaved={isSaved}/>
    </main>
  )
}

export async function getServerSideProps({ query }) {
  const { id } = query;

  const [ getDetail ] = await Promise.all([
    fetch(baseUrl, reqOptions(getAnimeDetail(id)))
  ]);

  const [ animeDetail ] = await Promise.all([
    getDetail.json()
  ]);

  return {
    props: {
      detail: animeDetail?.data?.Media || null,
    }
  };
}
