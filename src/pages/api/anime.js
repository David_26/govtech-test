export const getAnimeList = (page = 1, genre, isPopular, perPage = 10) =>
  JSON.stringify({
    query: `
      query ($page: Int = ${page}, $genre: String = "${genre}", $sort: [MediaSort] = ${isPopular || "[]"}, $perPage: Int = ${perPage}) {
        Page(page: $page, perPage: $perPage) {
          pageInfo{
            perPage
            currentPage
            hasNextPage
          }
          media (genre: $genre, sort: $sort, type: ANIME) {
            id
            title {
              romaji
              english
              native
              userPreferred
            }
            genres
            status
            averageScore
            coverImage {
              extraLarge
              large
              medium
              color
            }
          }
        }
      }
      `,
      variables: {}
  });

export const getAnimeDetail = (id) =>
  JSON.stringify({
    query: `
      query ($id: Int = ${id}) {
        Media (id: $id, type: ANIME) {
          id
          title {
            romaji
            english
            native
            userPreferred
          }
          coverImage {
            extraLarge
            large
            medium
            color
          }
          status
        description
        genres
        averageScore
        seasonYear
        episodes
        }
      }`
  });

export const getGenres = () =>
  JSON.stringify({
    query: `
      query {
        GenreCollection
      }  
    `
  })


export const reqOptions = (query) => {
  return {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    },
    redirect: 'follow',
    body: query
  };
};

export const baseUrl = "https://graphql.anilist.co/";