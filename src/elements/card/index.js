import React from 'react';
import Image from 'next/image'
import Link from 'next/link'
// import { Card } from 'flowbite-react'

const Card = ({data}) => {
  return (
    <Link href={`/anime/${data.id}`}>
      <div className="cursor-pointer bg-white rounded-[10px] h-full drop-shadow-md overflow-hidden hover:border-solid hover:border-2 hover:border-sky-500 ">
        <div className="grid h-full grid-cols-5">
          <div className="col-span-2">
            <div className="relative w-full h-60 md:rounded-none rounded-full mx-auto">
              <Image
                src={data?.coverImage?.extraLarge || "/01.jpg"}
                alt={data?.title?.romaji}
                fill
                style={{objectFit: 'cover', objectPosition: 'center center'}}
                priority
                sizes="(max-width: 768px) 100vw,
                  (max-width: 1200px) 50vw,
                  100vw"
              />
            </div>
          </div>
          <div className="col-span-3">
            <div className="flex items-start justify-start h-full bg-white">
              <div className="px-[30px] space-y-6 flex flex-col justify-center">
                <div>
                  <h5 className="text-xl font-bold tracking-tight text-gray-900 mb-2">
                    {data?.title?.romaji}
                  </h5>
                  <span className="bg-cyan-500 py-1 px-2 mr-3 text-xs md:text-md text-white rounded-md">{data?.status}</span>
                  <p className="text-slate-500 text-xs md:text-md mt-2">{data?.genres?.join(", ")}</p>
                  <div className="flex pt-1">
                    <Image
                      src="/star.svg"
                      alt="rating"
                      width="16"
                      height="16"
                    />
                    <p className="ml-1 text-md md:text-lg text-black">{data?.averageScore}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Link>
  )
}

export default Card