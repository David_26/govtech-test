const Skeleton = () => {
  return(
    <div className="animate-pulse mb-5">
      <div className="rounded-lg bg-slate-400 h-52 w-full"></div>
    </div>
  )
}
export default Skeleton