import React from 'react';
import Image from 'next/image';
import { useEffect, useState } from 'react';
import BackButton from '@elements/backButton';

const Cover = ({ data, handleBookmark, isSaved }) => {
  const [animeHasSaved, setAnimeHasSaved] = useState(false)

  useEffect(() => {
    setAnimeHasSaved(isSaved)
  },[isSaved])

  return(
    <div className="flex flex-col space-y-2 md:space-y-5 md:p-16 lg:h-full align-center lg:justify-center lg:pb-12">
      <div className="absolute top-0 left-0 -z-10 h-full w-screen">
        <Image
          src={data?.CoverImage?.extraLarge || "https://images6.alphacoders.com/101/1012987.jpg"}
          alt={data?.title?.romaji || 'anime'}
          fill
          style={{objectFit: 'cover', objectPosition: 'center center'}}
        />
      </div>
      <div className="lg:w-1/2 backdrop-blur-sm bg-black/30 p-5 md:p-8 md:rounded-lg">
        <BackButton className="mb-8" />
        <h1 className="text-2xl font-bold md:text-3xl lg:text-5xl text-white">
          {data?.title?.romaji}
        </h1>
        <div className="mt-5 md:flex md:items-center">
          <span className="bg-cyan-500 py-1 px-2 mr-3 text-xs md:text-md text-white rounded-md">{data?.status}</span>
          <p className="px-2 text-white text-xxs md:text-lg mt-3 md:mt-0">{data?.seasonYear}</p>
          <p className="px-2 text-white text-md md:text-lg mt-1 md:mt-0">{data?.genres.join(", ")}</p>
          <div className="flex items-center">
            <p className="px-2 text-white text-md md:text-lg md:mt-0">{data?.episodes} Episodes</p>
            <div className="flex md:backdrop-blur-md md:rounded-md p-1">
              <Image
                src="/star.svg"
                alt="rating"
                width="16"
                height="16"
              />
              <p className="ml-1 text-md md:text-lg text-white">{data?.averageScore}</p>
            </div>
          </div>
        </div>
        <p className="max-w-xs text-xs md:max-w-lg md:text-sm lg:max-w-2xl lg:text-sm text-white mt-2">
          {data?.description}
        </p>
        <div className="flex pt-6">
          <button className="inline-flex items-center h-14 px-8 mr-3 font-semibold group rounded-lg border border-slate-200 hover:bg-white text-white hover:text-slate-700 text-lg">
          <svg className="fill-white group-hover:fill-slate-700 w-6 h-6 mr-3" xmlns="http://www.w3.org/2000/svg" fillRule="evenodd" clipRule="evenodd"><path d="M23 12l-22 12v-24l22 12zm-21 10.315l18.912-10.315-18.912-10.315v20.63z"/></svg>
            Play
          </button>
          <button
            onClick={() => handleBookmark(data, isSaved)}
            className={`${animeHasSaved ? "bg-red-500" : ""} inline-flex items-center h-14 px-4 group font-semibold rounded-lg border border-slate-200 hover:bg-white text-white hover:text-slate-700 text-lg`}
          >
            <svg className="fill-white group-hover:fill-slate-700 w-6 h-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 16">
              <path d="M2 2a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v13.5a.5.5 0 0 1-.777.416L8 13.101l-5.223 2.815A.5.5 0 0 1 2 15.5V2zm2-1a1 1 0 0 0-1 1v12.566l4.723-2.482a.5.5 0 0 1 .554 0L13 14.566V2a1 1 0 0 0-1-1H4z"/>
            </svg>
          </button>
        </div>
      </div>
    </div>
  )
}

export default Cover