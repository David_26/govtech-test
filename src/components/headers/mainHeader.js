import React, { useState } from 'react';
import Link from 'next/link'

const MainHeader = ({filterData, handleChangeGenre, myBookmark}) => {
  const [navbarOpen, setNavbarOpen] = useState(false);

  return(    
    <nav className="flex z-20 flex-wrap items-center justify-between px-2 py-3 bg-gray-900 mb-3 fixed top-0 left-0 right-0">
        <div className="container px-4 md:mx-auto md:flex md:flex-wrap md:items-center md:justify-between">
          <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
            <Link href={`/`}>
              <h2 className="text-sm font-bold leading-relaxed inline-block mr-4 py-2 whitespace-nowrap uppercase text-white">Govtech Edu Test</h2>
            </Link>
            <button
              className="text-white cursor-pointer text-xl leading-none px-3 py-1 border border-solid border-transparent rounded bg-transparent block lg:hidden outline-none focus:outline-none"
              type="button"
              onClick={() => setNavbarOpen(!navbarOpen)}
            >
              <svg className="w-6 h-6" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clipRule="evenodd"></path></svg>
            </button>
          </div>
          {myBookmark &&
            <h2 className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-white">Saved Anime</h2>
          }
          {filterData?.length > 0 &&
            <div
              className={
                "md:flex md:flex-grow flex-col items-center" +
                (navbarOpen ? " flex" : " hidden")
              }
              id="example-navbar-danger"
            >
              <div className="block md:hidden w-full my-3">
                <select
                  className="w-full bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  onChange={(e) => handleChangeGenre(e.target.value)}
                >
                  {
                    filterData.map((item, index) => (
                      <option key={index} value={item}>{item}</option>    
                    ))
                  }
                </select>
              </div>
              <ul className="flex flex-col lg:flex-row list-none lg:ml-auto">
                <li className="nav-item self-center">
                  <Link href={`/trending`}>
                    <h2 className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-white hover:opacity-75">Trending</h2>
                  </Link>
                </li>
                <li className="nav-item self-center">
                  <Link href={`/bookmark`}>
                    <h2 className="px-3 py-2 flex items-center text-xs uppercase font-bold leading-snug text-white hover:opacity-75">Bookmark</h2>
                  </Link>
                </li>
                <li className="hidden md:block">
                  <select
                    className="w-full bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                    onChange={(e) => handleChangeGenre(e.target.value)}
                  >
                    {
                      filterData.map((item, index) => (
                        <option key={index} value={item}>{item}</option>    
                      ))
                    }
                  </select>
                </li>
              </ul>
            </div>
          }
        </div>
      </nav>

   
  )
}

export default MainHeader